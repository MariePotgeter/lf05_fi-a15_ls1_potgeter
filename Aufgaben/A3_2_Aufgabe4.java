import java.util.Scanner;
public class A3_2_Aufgabe4 {

    public static void main(String[] args){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Bitte geben Sie Kantenlänge des Würfels ein: ");
        double a = myScanner.nextDouble();
        System.out.println("Das Volumen des Würfels ist: " + wuerfel(a));
        System.out.println("Bitte geben sie die Kantenlänge des Quaders ein: ");
        double b = myScanner.nextDouble();
        double c = myScanner.nextDouble();
        double d = myScanner.nextDouble();
        System.out.println("Das Volumen des Quaders ist: " + quader(b, c, d));
    }

    public static double wuerfel(double a){
        return a * a * a;
    }

    public static double quader(double b, double c, double d){
        return b * c * d;
    }

}
