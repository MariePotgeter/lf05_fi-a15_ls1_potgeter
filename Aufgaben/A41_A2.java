import java.util.Scanner;

public class A41_A2 {

    public static void main(String[] args){

        Scanner tastatur = new Scanner(System.in);

        char steuersatz;
        double nettoWert;
        double bruttoWert;

        while (true) {
            System.out.print("Geben Sie den Nettowert ein: ");
            nettoWert = tastatur.nextDouble();

            System.out.print("Ermaeßigter Steuersatz j/n: ");
            steuersatz = tastatur.next().charAt(0);

            if (steuersatz == 'j'){
                bruttoWert = nettoWert + (nettoWert * 7 / 100); //nettowert * 1,07
                System.out.print(bruttoWert + " Euro");
                break;
            }
            if (steuersatz == 'n') {
                bruttoWert = nettoWert + (nettoWert * 19 / 100); // nettowert * 1,19
                System.out.print(bruttoWert + " Euro");
                break;
            }
        }
    }
}
