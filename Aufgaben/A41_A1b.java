public class A41_A1b {

    public static void main(String[] args){
        int zahl1 = 15;
        int zahl2 = 10;
        int zahl3 = 5;

        if(zahl1 > zahl2 && zahl1 > zahl3){
            System.out.println("Zahl1 ist größer.");
        }
        if(zahl3 > zahl2 || zahl3 > zahl1){
            System.out.println("Zahl3 ist größer.");
        }
        if(zahl3 > zahl1 && zahl3 > zahl2){
            System.out.println("Größte Zahl3");
        }
        else if(zahl2 > zahl1 && zahl2 > zahl3){
            System.out.println("Größte Zahl2");
        }else{
            System.out.println("Größte Zahl1");
        }


    }
}
