import java.util.Scanner;

class Fahrkartenautomat
{

    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        do {
            double zuZahlenderBetrag;
            double eingezahlterGesamtbetrag;
            double eingeworfeneMuenze;
            double rueckgabebetrag;
            int anzahlTickets;


            zuZahlenderBetrag = fahrkartenbestellungErfassen();
            eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
            rueckgeldAusgeben(rueckgabebetrag);



            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir wuenschen Ihnen eine gute Fahrt.");
        } while (true);
    };

    public static double fahrkartenbestellungErfassen(){
        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        int anzahlTickets;
       // double  ticketPreis;
        int wahl;

        System.out.println("Fahrkartenbestellvorgang:");
        System.out.println("===========================\n");
        System.out.println("Waehlen Sie ihre Wunschfahrkarte f�r Berlin ABC aus:");

        String[] tarif = new String[10];
        tarif[0] = "Einzelfahrschein Regeltarif AB";
        tarif[1] = "Einzelfahrschein Regeltarif BC";
        tarif[2] = "Einzelfahrschein Regeltarif ABC";
        tarif[3] = "Kurzstrecke";
        tarif[4] = "Tageskarte Regeltarif AB";
        tarif[5] = "Tageskarte Regeltarif BC";
        tarif[6] = "Tageskarte Regeltarif ABC";
        tarif[7] = "Kleingruppen-Tageskarte Regeltarif AB";
        tarif[8] = "Kleingruppen-Tageskarte Regeltarif BC";
        tarif[9] = "Kleingruppen-Tageskarte Regeltarif ADC";


        double[] ticketPreise = new double[10];
        ticketPreise[0] = 3.00;
        ticketPreise[1] = 3.30;
        ticketPreise[2] = 3.60;
        ticketPreise[3] = 1.90;
        ticketPreise[4] = 8.80;
        ticketPreise[5] = 9.00;
        ticketPreise[6] = 9.60;
        ticketPreise[7] = 25.50;
        ticketPreise[8] = 26.30;
        ticketPreise[9] = 26.90;

        for (int i = 0; i < ticketPreise.length; i++){
            System.out.println(tarif[i] + " " + ticketPreise[i] + " Euro" + " (" + i + ")");
        }
        double ticketPreis;

        System.out.println("Ihre Wahl");
        wahl = tastatur.nextInt();



        while(wahl < 1 || wahl > tarif.length ){

            System.out.println(">>falsche Eingabe<<");
            System.out.println("Ihre Wahl");
            wahl = tastatur.nextInt();
        }


        ticketPreis = ticketPreise[wahl];

    System.out.println("Anzahl der Tickets: ");
    anzahlTickets = tastatur.nextInt();


        if(anzahlTickets > 10 || anzahlTickets <= 0){
            System.out.println("Ungültige Anzahl der Tickets. Maximale Ticketanzahl : 10. Anzahl wurde auf 1 gesetzt.");
            anzahlTickets = 1;
        }
        else if(anzahlTickets == 1){
           System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 2){
            System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 3){
            System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 4){
            System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 5){
            System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 6){
            System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 7){
            System.out.println("Anzahl der Tickets beträgt " + anzahlTickets);
        }
        else if (anzahlTickets == 8){
            System.out.println("Anzahl der Tickets beträgt" + anzahlTickets);
        }
        else if (anzahlTickets == 9){
            System.out.println("Anzahl der Tickets beträgt" + anzahlTickets);
        }
        else{
            System.out.println("Anzahl der Tickets beträgt" + anzahlTickets);
        }

        double gesamtPreis = ticketPreis * anzahlTickets;
        return gesamtPreis;
    }


    public static double fahrkartenBezahlen(double zuZahlenderBetrag){
        Scanner tastatur = new Scanner(System.in);

        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;

        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben(){
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
     public static void rueckgeldAusgeben(double rueckgabebetrag){
         if(rueckgabebetrag > 0.0)
         {
             System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f EURO\n" , rueckgabebetrag);
             System.out.println("wird in folgenden Muenzen ausgezahlt:");

             while(rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
             {
                 System.out.println("2 EURO");
                 rueckgabebetrag -= 2.0;
             }
             while(rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
             {
                 System.out.println("1 EURO");
                 rueckgabebetrag -= 1.0;
             }
             while(rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
             {
                 System.out.println("50 CENT");
                 rueckgabebetrag -= 0.5;
             }
             while(rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
             {
                 System.out.println("20 CENT");
                 rueckgabebetrag -= 0.2;
             }
             while(rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
             {
                 System.out.println("10 CENT");
                 rueckgabebetrag -= 0.1;
             }
             while(rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
             {
                 System.out.println("5 CENT");
                 rueckgabebetrag -= 0.05;
             }
         }
     }
}

