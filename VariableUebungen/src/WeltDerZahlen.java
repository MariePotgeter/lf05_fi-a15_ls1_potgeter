
		/**
		  *   Aufgabe:  Recherechieren Sie im Internet !
		  * 
		  *   Sie dürfen nicht die Namen der Variablen verändern !!!
		  *
		  *   Vergessen Sie nicht den richtigen Datentyp !!
		  *
		  *
		  * @version 1.0 from 21.08.2019
		  * @author << Ihr Name >>
		  */

		public class WeltDerZahlen {

		  public static void main(String[] args) {
		    
		    /*  *********************************************************
		    
		         Zuerst werden die Variablen mit den Werten festgelegt!
		    
		    *********************************************************** */
		    // Im Internet gefunden ?
		    // Die Anzahl der Planeten in unserem Sonnesystem                    
		    int anzahlPlaneten =  8;
		    
		    // Anzahl der Sterne in unserer Milchstraße
		      long  anzahlSterne = 200000000000l;
		    
		    // Wie viele Einwohner hat Berlin?
		    int bewohnerBerlin = 3664088;
		    
		    // Wie alt bist du?  Wie viele Tage sind das?
		    
		      int alterTage = 11956;
		    
		    // Wie viel wiegt das schwerste Tier der Welt?
		    // Schreiben Sie das Gewicht in Kilogramm auf!
		      int gewichtKilogramm =  150000 ;
		    
		    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
		       int flaecheGroessteLand = 17098242;
		    
		    // Wie groß ist das kleinste Land der Erde?
		    
		       double flaecheKleinsteLand = 0.44044;
		    
		    
		    
		    
		    /*  *********************************************************
		    
		         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		    
		    *********************************************************** */
		    
		    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
		    
		    System.out.println("Anzahl der Sterne: " + anzahlSterne);
		    
		    System.out.println("Anzahl der Einwohner in Berlin: " + bewohnerBerlin);
		    
		    System.out.println("Mein Alter in Tagen: " + alterTage);
		    
		    System.out.println("Das schwerste Tier der Welt wiegt: " + gewichtKilogramm);
		    
		    System.out.println("Größe des größten Landes der Erde: " + flaecheGroessteLand);
		    
		    System.out.println("Größe des kleinsten Landes der Erde: " + flaecheKleinsteLand);
		    
		    
		     System.out.println(" *******  Ende des Programms  ******* ");
		    
		  }
		}



