
/**
 * Variablen.java Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
 * 
 * @author
 * @version
 */
public class Variablen {
	public static void main(String[] args) {
		/*
		 * 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine
		 * geeignete Variable
		 */
		long zaehlen;

		/*
		 * 2. Weisen Sie dem Zaehler den Wert 25 zu und geben Sie ihn auf dem Bildschirm
		 * aus.
		 */
		zaehlen = 25;
		System.out.println("Zähler: " + zaehlen);

		/*
		 * 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt eines Programms
		 * ausgewaehlt werden. Vereinbaren Sie eine geeignete Variable
		 */
		char buchstabe = 'a';

		/*
		 * 4. Weisen Sie dem Buchstaben den Wert 'C' zu und geben Sie ihn auf dem
		 * Bildschirm aus.
		 */
		buchstabe = 'C';
		System.out.println("Buchstabe: " + buchstabe);

		/*
		 * 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
		 * notwendig. Vereinbaren Sie eine geeignete Variable
		 */
		double astronomie;

		/*
		 * 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu und geben Sie sie
		 * auf dem Bildschirm aus.
		 */
		astronomie = 299792458;
		System.out.println("Lichtgeschwindigkeit: " + astronomie);

		/*
		 * 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung soll
		 * die Anzahl der Mitglieder erfasst werden. Vereinbaren Sie eine geeignete
		 * Variable und initialisieren sie diese sinnvoll.
		 */
		byte mitglieder;

		/* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus. */
		mitglieder = 7;
		System.out.println("Mitglieder: " + mitglieder);

		/*
		 * 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
		 * Vereinbaren Sie eine geeignete Variable und geben Sie sie auf dem Bildschirm
		 * aus.
		 */
		final double elementarladung = 0000000000000000000.1602;
		System.out.println("elektrische Elementarladung: " + elementarladung);

		/*
		 * 10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
		 * Vereinbaren Sie eine geeignete Variable.
		 */
		boolean zahlung;

		/*
		 * 11. Die Zahlung ist erfolgt. Weisen Sie der Variable den entsprechenden Wert
		 * zu und geben Sie die Variable auf dem Bildschirm aus.
		 */
		zahlung = true;
		System.out.println("AusgabeErfolgt: " + zahlung);

	}// main
}// Variablen
